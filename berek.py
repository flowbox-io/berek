#!/usr/bin/env python3
import argparse
import os
import subprocess
from berek.src.git_utils import Git
from berek.src.parsers import Parsers
from berek.src.shell_utils import get_editor


class BerekMethodNotImplemented(RuntimeError):
    pass


def build_parser():
    """Builds command-line argument parser."""

    args_parser = argparse.ArgumentParser()
    args_subparsers = args_parser.add_subparsers()

    list_subparser = args_subparsers.add_parser('list-formats', aliases=['list', 'lst', 'l'])
    list_subparser.set_defaults(func=main_list)

    make_subparser = args_subparsers.add_parser('make', aliases=['mk', 'm'])
    make_subparser.set_defaults(func=main_make)
    make_subparser.add_argument('tagname',
                                help="Tag name to create. Suggested name scheme: vMAJOR.MINOR.PATCH "
                                     "(cf. semantic versioning).")
    make_subparser.add_argument('-n', '--dry-run', action='store_true')
    make_subparser_steps_group = make_subparser.add_mutually_exclusive_group(required=False)
    make_subparser_steps_group.add_argument('-o', '--files-only', action='store_true',
                                            help='when used, berek will only create output files.')
    make_subparser_steps_group.add_argument('-t', '--tag-only', action='store_true',
                                            help='when used, '
                                                 'berek will use previously generated output files for tagging')
    make_subparser.add_argument('-e', '--editor', action='store_true',
                                help='use if you want to edit Changelog before tagging')
    make_subparser.add_argument('-r', '--repository-path', default=os.getcwd())
    make_subparser.add_argument('-a', '--artifact-path')
    make_subparser.add_argument('--since')

    return args_parser


def main_make(args):
    """Called when 'make' command is used."""
    if args.tag_only:
        files_step, tag_step = False, True
    elif args.files_only:
        files_step, tag_step = True, False
    else:
        files_step, tag_step = True, True

    repo_path = os.path.abspath(args.repository_path)
    git = Git(repo_path)

    repo_root_path = os.path.join(repo_path, '.git')
    if args.artifact_path:
        artifact_path = args.artifact_path
    else:
        artifact_path = os.path.join(repo_root_path, 'berek-msgs')

    if not os.path.exists(artifact_path):
        os.makedirs(artifact_path)

    def ensure_dir_exists(dir_path):
        if not os.path.exists(dir_path):
            # CAVEAT
            # Documentation states that os.makedirs is just stupid:
            #     makedirs() will become confused if the path elements
            #     to create include pardir (eg. ”..” on UNIX systems).
            # So one needs to call abspath manually...
            dir_path = os.path.abspath(dir_path)
            os.makedirs(dir_path)
        else:
            if not os.path.isdir(dir_path):
                raise NotADirectoryError("{} exists, but is not a directory".format(dir_path))

    if files_step:

        if args.since:
            commit = args.since
        else:
            commit = git.get_previous_tag(ignore_errors=True)
            if commit is None:
                print('Seems that no previous tag was given. Selecting oldest commit in repo')
                commit = git.rev_list('--max-parents=0', 'HEAD').strip()
            print('Last found tag: {}'.format(commit))

        changelog = git.get_changelog_since(commit)
        commits_all = git.get_commit_messages_since(commit)
        commits_used = git.get_parsed_commits_since(commit)

        artifact = [
            ('changelog.txt', changelog),
            ('all-commits.txt', commits_all),
            ('changelog-commits.txt', commits_used),
        ]

        for path, string in artifact:
            ensure_dir_exists(artifact_path)
            path = os.path.join(artifact_path, path)
            with open(path, 'w+') as f:
                f.write(string)

        print('\nWrote output files to: {}'.format(artifact_path))
    else:
        with open(os.path.join(artifact_path, 'changelog.txt'), 'r') as f:
            changelog = f.read()

    if args.editor:
        editor = get_editor()
        cmd = [editor, os.path.join(artifact_path, 'changelog.txt')]
        subprocess.call(cmd)
        with open(os.path.join(artifact_path, 'changelog.txt'), 'r') as f:
            changelog = f.read()

    if tag_step:
        print('\nChangelog:\n{}'.format('-'*80))
        print('{}\n'.format(changelog))

        if args.dry_run:
            print('--dry-run option set, no tagging to be done')
        else:
            print('tagging')
            git.set_tag(args.tagname, changelog)


def main_list(args):
    """Called when 'list-formats' command is used."""
    parsers = Parsers.get_parsers()
    for parser in parsers:
        print(parser.print_format())


def main():
    """Parses command-line arguments and dispatches the execution."""
    parser = build_parser()
    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
