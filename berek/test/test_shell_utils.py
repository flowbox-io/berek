#!/usr/bin/env python3
import unittest
import subprocess
from berek.test.base_test import Test

from berek.src.shell_utils import run_command


class TestParsers(Test):
    def test_bash_utils_dummy(self):
        command = 'echo a'.split()
        self.assertEqual(run_command(command), 'a')

    def test_bash_utils_exception(self):
        command = ['false']
        with self.assertRaises(subprocess.CalledProcessError):
            run_command(command)


if __name__ == '__main__':
    unittest.main()
