#!/usr/bin/env python3
import unittest

from berek.test.base_test import Test
from berek.src.parsers import Parsers


class TestBugT1191(Test):

    @Test.with_parser_expectations(test_id=1, test_class='bugz')
    def test_T1191_1(self, commit_raw, commit_parsed):
        self.assertEqual(commit_parsed, Parsers.parse_commit(commit_raw))

    @Test.with_parser_expectations(test_id=2, test_class='bugz')
    def test_T1191_2(self, commit_raw, commit_parsed):
        self.assertEqual(commit_parsed, Parsers.parse_commit(commit_raw))


if __name__ == '__main__':
    unittest.main()