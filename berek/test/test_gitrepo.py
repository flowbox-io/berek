import tempfile
import shutil
import errno

from berek.src.git_repo import GitRepo, InvalidPathException
from berek.test.base_test import TestWithGitRepo


class TestGitRepo(TestWithGitRepo):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.test_repo_variant_id = 1

    def setUp(self):
        super().setUp()
        self.temporary_invalid_directory = tempfile.mkdtemp()

    def tearDown(self):
        try:
            shutil.rmtree(self.temporary_directory)
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise e
        except TypeError:
            pass
        finally:
            super().tearDown()

    def test_status(self):
        git_repo = GitRepo(repo_dir=self.temporary_directory)

        log_n1 = str(git_repo.log("-n1")).splitlines()
        self.assertEqual(
            "Author: John Zażółć Gęślą Jaźń 🔥⽕ Doe <john+somethingsomething@localhost>",
            log_n1[1]
        )
        self.assertIn(
            "commit3",
            log_n1[4]
        )

    def test_invalid_path(self):
        with self.assertRaises(InvalidPathException):
            git_repo = GitRepo(repo_dir=self.temporary_invalid_directory)
