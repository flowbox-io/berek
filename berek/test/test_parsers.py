#!/usr/bin/env python3
import unittest

from berek.src.parsers import Parsers
from berek.test.base_test import Test


class TestParsers(Test):

    @Test.with_parser_expectations(test_id=1, test_class='parsers')
    def test_parse_commit_1(self, commit_raw, commit_parsed):
        self.assertEqual(commit_parsed, Parsers.parse_commit(commit_raw))

    @Test.with_parser_expectations(test_id=2, test_class='parsers')
    def test_parse_commit_2(self, commit_raw, commit_parsed):
        self.assertEqual(commit_parsed, Parsers.parse_commit(commit_raw))

    @Test.with_parser_expectations(test_id=3, test_class='parsers', use_parsed_commit='')
    def test_parse_commit_3(self, commit_raw, commit_parsed):
        self.assertEqual(commit_parsed, Parsers.parse_commit(commit_raw))

    @Test.with_parser_expectations(test_id=4, test_class='parsers')
    def test_parse_commit_4(self, commit_raw, commit_parsed):
        self.assertEqual(commit_parsed, Parsers.parse_commit(commit_raw))

    @Test.with_parser_expectations(test_id=5, test_class='parsers')
    def test_parse_commit_5(self, commit_raw, commit_parsed):
        self.assertEqual(commit_parsed, Parsers.parse_commit(commit_raw))


if __name__ == '__main__':
    unittest.main()
