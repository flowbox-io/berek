#!/usr/bin/env python3
import unittest

from berek.src.utils import cwd
from berek.test.base_test import TestWithGitRepo

from berek.src.shell_utils import run_command
from berek.src.git_utils import Git


class TestGitUtils(TestWithGitRepo):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.test_repo_variant_id = 1

    def test_git_utils_previous_tag(self):
        tag = self.load_string_from_file('test_git_commands_1_previous_tag')
        repository_path = self.git_repo.repo_dir
        git = Git(repository_path)
        self.assertEqual(git.get_previous_tag(), tag)

    def test_git_utils_get_commit_messages_since_given_tag(self):
        tag = self.load_string_from_file('test_git_commands_1_previous_tag')
        commits = self.load_string_from_file('test_git_commands_1_commits_since_given_tag')
        repository_path = self.git_repo.repo_dir
        git = Git(repository_path)
        self.assertEqual(git.get_commit_messages_since(tag), commits)

    def test_git_utils_get_changelogs_since_given_tag_empty(self):
        tag = self.load_string_from_file('test_git_commands_1_previous_tag')
        repository_path = self.git_repo.repo_dir
        git = Git(repository_path)
        self.assertEqual(git.get_changelog_since(tag), '')

    def test_git_utils_create_tag(self):
        tag = 'test_git_utils_create_tag'
        repository_path = self.git_repo.repo_dir
        git = Git(repository_path)
        git.set_tag(tag, 'msg')
        with cwd(repository_path):
            self.assertEqual(run_command('git tag -l --contains HEAD'.split()), tag)


class TestGitUtils2(TestWithGitRepo):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.test_repo_variant_id = 2

    def test_get_commit_messages_since_given_commit(self):
        self.commit_number = self.git_repo.log('-3', '--format=%H').splitlines()[-1]
        repository_path = self.git_repo.repo_dir
        git = Git(repository_path)
        with cwd(repository_path):
            commits = self.load_string_from_file('test_git_commands_2_commits_since_given_commit')
            self.assertEqual(git.get_commit_messages_since(self.commit_number), commits)


if __name__ == '__main__':
    unittest.main()
