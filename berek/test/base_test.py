import shutil
import tempfile
import unittest
import os

import subprocess

import errno

from berek.src.git_repo import GitRepo
from berek.src.utils import cwd


class Test(unittest.TestCase):
    @classmethod
    def with_parser_expectations(cls, test_id=None,
                                 use_parsed_commit=None,
                                 test_class=None):
        """Wrapper that provides test case with commit_raw & commit_parsed read from file.

        Reads appropriate files to obtain raw commit message and parsed commit message. Then provides them to the
        wrapped method as commit_raw & commit_parsed parameters.

        :param test_id: The ID of test file.
        :type test_id: int

        :param use_parsed_commit: If not None, this value is used as commit_parsed (instead of being read from file).
        :type use_parsed_commit: None | str
        """

        commit_raw_filename_template = 'test_{test_class}_commit_{test_id}'
        commit_parsed_filename_template = 'test_{test_class}_commit_{test_id}_parsed'

        commit_raw_filename = commit_raw_filename_template.format(test_id=test_id, test_class=test_class)
        commit_parsed_filename = commit_parsed_filename_template.format(test_id=test_id, test_class=test_class)

        def expected_parse_test_fun_wrapper(test_fun):
            def expected_parse_test_fun_runner(self):

                commit_raw = Test.load_string_from_file(commit_raw_filename)

                if use_parsed_commit is None:
                    commit_parsed = Test.load_string_from_file(commit_parsed_filename)
                else:
                    commit_parsed = use_parsed_commit

                return test_fun(self, commit_raw=commit_raw, commit_parsed=commit_parsed)

            return expected_parse_test_fun_runner

        return expected_parse_test_fun_wrapper

    @staticmethod
    def load_string_from_file(filename):
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test_files', filename)
        with open(path, encoding="utf-8") as f:
            return f.read()

    @staticmethod
    def get_test_workspace_dir():
        return os.path.join(os.path.dirname(os.path.realpath(__file__)), 'workspace')


class TestWithGitRepo(Test):
    def setUp(self):

        assert str(self.test_repo_variant_id), \
            "Could not use 'self.test_repo_variant_id'. Have you set it in your Test's __init__?"

        self.temporary_directory = tempfile.mkdtemp()
        try:

            repo_commands = self.load_string_from_file(
                'test_git_commands_{}'.format(str(self.test_repo_variant_id))
            )
            repo_commands = repo_commands.splitlines()

            with cwd(self.temporary_directory):
                for command in repo_commands:
                    subprocess.check_output(command,
                                            shell=True,
                                            stderr=subprocess.STDOUT)

            self.git_repo = GitRepo(repo_dir=self.temporary_directory)

        except Exception:
            self.tearDown()
            raise

    def tearDown(self):
        try:
            shutil.rmtree(self.temporary_directory)
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise e

