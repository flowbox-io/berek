import subprocess
import os


def run_command(command):
    return subprocess.check_output(command, stderr=subprocess.PIPE, universal_newlines=True).rstrip()


def get_editor():
    def editor_git_env(): return os.environ.get('GIT_EDITOR', "")

    def editor_core_editor(): return subprocess.check_output(['git', 'config', 'core.editor']).strip()

    def editor_visual_env(): return os.environ.get('VISUAL', "")

    def editor_editor_env(): return os.environ.get('EDITOR', "")

    def editor_default(): return "vi"

    return editor_git_env() or editor_core_editor() or editor_visual_env() or editor_editor_env() or editor_default()
