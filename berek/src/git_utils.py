import os

import subprocess

from berek.src.parsers import Parsers
from berek.src.utils import cwd
from berek.src.git_repo import GitRepo


class Git(GitRepo):
    def __init__(self, repository_path, git_path='git'):
        super().__init__(git_command=git_path, repo_dir=repository_path)
        self.git_path = git_path
        self.repository_path = repository_path

    def get_previous_tag(self, ignore_errors=False):
        try:
            with cwd(self.repository_path):
                return self.describe('--tags', '--abbrev=0').strip()
        except subprocess.CalledProcessError as e:
            if ignore_errors:
                return None
            raise e

    def get_parsed_commits_since(self, ref):
        commit_messages = self.get_commit_messages_list_since(ref)

        commit_list = [
            commit
            for commit in commit_messages
            if Parsers.parse_commit(commit) != ''
        ]

        commits = '\n'.join(commit_list)

        return commits.rstrip()

    def get_commit_messages_list_since(self, ref):
        commits = self.rev_list('--reverse', 'HEAD', '^{}'.format(ref))

        commit_messages = [
            self.log('--format=%B', '-n1', line).strip()
            for line in commits.splitlines()
        ]

        return commit_messages

    def get_commit_messages_since(self, ref):
        commit_messages = '\n'.join(self.get_commit_messages_list_since(ref))
        return commit_messages.strip()

    def get_changelog_since(self, ref):
        commit_messages = self.get_commit_messages_list_since(ref)

        changelog_list = [
            Parsers.parse_commit(commit)
            for commit in commit_messages
        ]

        changelogs = '\n'.join(
            [
                changelog
                for changelog in changelog_list
                if changelog != ''
            ]
        )

        return changelogs.strip()

    def set_tag(self, tag, msg):
        changelog_file = os.path.join(self.repository_path,
                                      'changelog')
        with open(changelog_file, 'w+') as f:
            f.write(msg)
        self.tag('-a', tag, '-F', 'changelog')
        os.remove(changelog_file)
