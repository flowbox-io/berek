from textwrap import dedent


class ParserInputValidationException(Exception):
    pass


class Parsers:
    @classmethod
    def get_parsers(cls):
        return cls.__subclasses__()

    @classmethod
    def parse_commit(cls, input_string):
        parsers = cls.get_parsers()
        for parser in parsers:
            try:
                return parser.parse(input_string).rstrip()
            except ParserInputValidationException:
                pass
        else:
            return ''


class Parser1(Parsers):
    @staticmethod
    def parse(input_string):
        changelog_index = input_string.lower().find('changelog\n')
        if changelog_index == -1:
            raise ParserInputValidationException
        changelog_index_end = changelog_index + len('changelog\n')
        return input_string[changelog_index_end:]

    @classmethod
    def print_format(cls):
        return dedent("""
                        -- {header}

                        Commit format:

                            <first_line>

                            <description>
                            Changelog
                            <the_changelog>

                        Generated changelog:

                            <the_changelog>
                        """.format(header=(cls.__name__ + ' ').ljust(80-3,  # account for '-- ' prefix
                                                                     '-')))


class Parser2(Parsers):
    @staticmethod
    def parse(input_string):
        changelog_index = input_string.lower().find('changelog')
        changelog_index_end = changelog_index + len('changelog')
        if changelog_index_end != len(input_string) or changelog_index == -1:
            raise ParserInputValidationException
        return input_string[:changelog_index].split('\n')[0]

    @classmethod
    def print_format(cls):
        return dedent("""
                        -- {header}

                        Commit format (variant A):

                            <the_changelog> Changelog

                        Commit format (variant B):
                            <the_changelog>

                            <description>
                            Changelog

                        Generated changelog:

                            <the_changelog>
                        """.format(header=(cls.__name__ + ' ').ljust(80-3,  # account for '-- ' prefix
                                                                     '-')))

