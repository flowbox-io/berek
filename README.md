[![Build Status](https://drone.io/bitbucket.org/flowbox-io/berek/status.png)](https://drone.io/bitbucket.org/flowbox-io/berek/latest)

Berek
=====

Berek (Polish for "tag") is a script that parses git history and generates the changelog. Commits are supposed to contain changelog entries in one of accepted formats.


Git commit message format for this repo
---------------------------------------

As described [here](http://karma-runner.github.io/0.13/dev/git-commit-msg.html):

    <type>(<scope>): <subject>
    
    <body>
    
    <footer>
    
Allowed `<type>` values:

- `feat`     - new feature for the user, not a new feature for build script
- `fix`      - bug fix for the user, not a fix to a build script
- `docs`     - changes to the documentation
- `style`    - formatting, missing semi colons, etc; no production code change
- `refactor` - refactoring production code, eg. renaming a variable
- `test`     - adding missing tests, refactoring tests; no production code change
- `dev`      - development feature, like helpers or wrappers; not a user feature nor simple refactor

Other `<type>` values that have currently no good use in this repo:

- `chore`    - updating grunt tasks etc; no production code change

Allowed `<scope>` values:

- `vcs`                  - git/hg/svn/cvs connectors
- `parser`               - commit message parser
- `generator`            - changelog generator
- `gitlab` / `gitlab-ci` - changes for gitlab's CI service

Some additional `<type>(<scope>)`:

- `test(runner)`         - changes to the test discovery module / runner
- `docs(README)`         - changes to this file
